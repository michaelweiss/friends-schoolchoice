# README #

### What is this repository for? ###

* This Repository contains the source code of the COMP9596 Research Project "School Choice with Friendships" of Michael Weiss at UNSW / NICTA (Code v0.1) will contain the later versions (v0.1+)
* v0.1

### How do I get set up? ###

* The Code is structured as  a Maven Project

### We are grateful for all types of contributions / feedback, e.g. ###
* Email Feedback / Questions
* Improvement Propositions / Bug Reports (to be submitted via issue tracker)
* Pull Requests

### Who do I talk to? ###

* Michael Weiss, weiss.michael@gmx.ch