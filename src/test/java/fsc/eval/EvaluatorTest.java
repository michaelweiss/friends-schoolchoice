/*
 * 
 * File: 	EvaluatorTest.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.eval;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

import fsc.TestSuite;
import fsc.mech.Allocation;
import fsc.model.School;
import fsc.model.Student;
import fsc.model.World;

public class EvaluatorTest {

    private static final int CLIQUESIZE = 3;
    private static final int NUMBEROFCLIQUES = 2;

    private Set<Student> clique1;
    private Set<Student> clique2;
    private World world;
    private List<School> schools;

    // Same utility for all students regarding friendships
    Map<Integer, BigDecimal> uFriends;
    // School utilities clique specific
    Map<School, BigDecimal> uSchoolsClique1;
    Map<School, BigDecimal> uSchoolsClique2;

    @Before
    public void init(){
        world = new World(CLIQUESIZE-1);
        // Create Schools
        schools = new ArrayList<School>();
        for(int i =0; i < 2; i++){
            School school = new School(3, world);
            schools.add(school);
            world.addSchool(school);
        }
        world.finalizeSchools();

        //Create Utility Functions
        uSchoolsClique1 = new HashMap<School, BigDecimal>();
        uSchoolsClique1.put(schools.get(0), BigDecimal.valueOf(1));
        uSchoolsClique1.put(schools.get(1), BigDecimal.valueOf(2));
        uSchoolsClique2 = new HashMap<School, BigDecimal>();
        uSchoolsClique2.put(schools.get(0), BigDecimal.valueOf(2));
        uSchoolsClique2.put(schools.get(1), BigDecimal.valueOf(1));

        uFriends = new HashMap<Integer, BigDecimal>();
        for(int i = 0; i < CLIQUESIZE; i++){
            uFriends.put(i, BigDecimal.valueOf(i));
        }
        // Clique 1	
        clique1 = new HashSet<Student>();
        for(int i = 0; i < CLIQUESIZE; i++){			
            Student student = new Student(world, uFriends, uSchoolsClique1);
            clique1.add(student);
            world.addStudent(student);
        }
        // Clique 2
        clique2 = new HashSet<Student>();
        for(int i = 0; i < CLIQUESIZE; i++){			
            Student student = new Student(world, uFriends, uSchoolsClique2);
            clique2.add(student);
            world.addStudent(student);
        }
        world.finalizeStudentList();

        // Generate Friendships
        for(Student student : clique1){			
            for(Student friend : clique1){
                if(student != friend) student.addFriend(friend);
            }
        }
        for(Student student : clique2){			
            for(Student friend : clique2){
                if(student != friend) student.addFriend(friend);
            }
        }

        world.finalizeAllStudents(TestSuite.rng.nextRNG());
    }

    @Test
    public void test(){
        System.out.println("EvaluatorTest");
        Allocation effective = effectiveAllocation();
        EvaluationReport eval = Evaluator.evaluate(effective, Integer.MIN_VALUE, "EVALUATOR TEST");

        // Total Percieved utility is 6*1 = 6;
        BigDecimal reportedWelfare = eval.getNormalizedTotalUtility();
        Assert.assertTrue(reportedWelfare + ", " + 6, 
                reportedWelfare.compareTo(BigDecimal.valueOf(6)) == 0);

        // Everybody should have 2 friends in friendslexicography
        Lexicography friendsLex = eval.getFriendsLexicography();
        Assert.assertEquals(0, friendsLex.getEntry(2));
        Assert.assertEquals(0, friendsLex.getEntry(1));
        Assert.assertEquals(6, friendsLex.getEntry(0));

        // Everybody should have most prefered school in friendslexicography
        Lexicography schoolLex = eval.getFriendsLexicography();
        Assert.assertEquals(0, schoolLex.getEntry(1));
        Assert.assertEquals(6, schoolLex.getEntry(0));
    }

    /**
     * Creates the most desirable allocation: Everyone at most desired school with all friends
     * @return
     */
    private Allocation effectiveAllocation(){
        Allocation result = new Allocation(world);
        for(Student student : clique1){
            result.allocate(student, schools.get(1));
        }
        for(Student student : clique2){
            result.allocate(student, schools.get(0));
        }
        return result;
    }


}
