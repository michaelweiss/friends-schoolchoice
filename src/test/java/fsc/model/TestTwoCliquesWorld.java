/*
 * 
 * File: 	TestTwoCliquesWorld.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package fsc.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import fsc.util.RNG;

public class TestTwoCliquesWorld implements WorldFactory {


    public static final int CLIQUE_SIZE = 3;

    @Override
    public World createWorld(RNG rng) {
        int k = 3;
        World world = new World(k);

        world.addSchool(new School(1, world));
        world.addSchool(new School(2, world));
        world.addSchool(new School(3, world));

        world.finalizeSchools();

        Random rand = new Random();
        Map<Integer, BigDecimal> uFriends = new HashMap<>();
        for(int i = 0; i<= k; i++){
            uFriends.put(i, BigDecimal.valueOf(i*1.5)); 
        }

        Map<School, BigDecimal> uSchools1 = new HashMap<>();
        Map<School, BigDecimal> uSchools2 = new HashMap<>();
        for(School school : world.getSchools()){
            uSchools1.put(school,BigDecimal.valueOf(school.getId()+1)); //Value of schools = increasing by 1 with each school 
            uSchools2.put(school,BigDecimal.valueOf(world.getSchools().size() - school.getId())); //Value of schools = increasing by 1 with each school 
        }

        List<Student> students1 = new ArrayList<>();
        for(int i = 0; i < CLIQUE_SIZE; i++){
            Map<School, BigDecimal> uSchoolsRand = new HashMap<>();
            for(School school : world.getSchools()){
                uSchoolsRand.put(school,BigDecimal.valueOf(rand.nextDouble())); //Value of schools = increasing by 1 with each school 
            }
            Student student = new Student(world, uFriends, uSchoolsRand);
            students1.add(student);
            world.addStudent(student);
        }

        List<Student> students2 = new ArrayList<>();
        for(int i = 0; i < CLIQUE_SIZE; i++){
            Student student = new Student(world, uFriends, uSchools2);
            students2.add(student);
            world.addStudent(student);
        }


        world.finalizeStudentList();


        for(Student student : students1){

            for(Student friend : students1){
                if(student != friend) student.addFriend(friend);
            }
            student.finalizeStudent(rng.nextRNG());
        }

        for(Student student : students2){
            for(Student friend : students2){
                if(student != friend) student.addFriend(friend);
            }
            student.finalizeStudent(rng.nextRNG());
        }

        return world;
    }



}
