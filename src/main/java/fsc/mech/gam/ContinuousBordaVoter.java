/*
 * 
 * File: 	ContinuousBordaVoter.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.mech.gam;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fsc.mech.gam.CBA.Cluster;
import fsc.mech.gam.CBA.RationalCluster;
import fsc.model.School;
import fsc.model.Student;
import fsc.util.ValuePair;


public class ContinuousBordaVoter implements Aggregator {

    @Override
    public Set<RationalCluster> generateSchoolPreferences(
            Set<Cluster> clusters) {
        Set<RationalCluster> result = new HashSet<CBA.RationalCluster>();
        for(Cluster cluster : clusters){
            List<School> ordinalPreferences = bordaCount(cluster);
            result.add(new RationalCluster(cluster, ordinalPreferences));
        }
        return result;
    }

    private static List<School> bordaCount(Cluster cluster) {
        if(cluster.size() == 0) throw new RuntimeException("No student in cluster");
        Map<School, BigDecimal> valueSums = new HashMap<>();
        for(School school : cluster.iterator().next().getWorld().getSchools()){
            valueSums.put(school, BigDecimal.ZERO);
        }
        for(Student student : cluster){
            for(Entry<School, BigDecimal> normEntry : normalizeValues(student.getuSchools()).entrySet()){
                BigDecimal newValue = valueSums.get(normEntry.getKey()).add(normEntry.getValue());
                valueSums.put(normEntry.getKey(), newValue);
            }
        }
        return ValuePair.ordinal(valueSums);
    }


    private static Map<School, BigDecimal> normalizeValues(Map<School, BigDecimal> values){
        Map<School, BigDecimal> normalizedValues = new HashMap<School, BigDecimal>();
        BigDecimal sum = BigDecimal.ZERO;
        for(BigDecimal value : values.values()){
            sum = sum.add(value);
        }
        if(sum.compareTo(BigDecimal.ZERO) == 0) {
            System.out.println("Attention: sum of values equals 0");
            return values;
        }
        BigDecimal normalizator = BigDecimal.ONE.divide(sum, 20, RoundingMode.HALF_UP);
        for(Entry<School, BigDecimal> valueEntry : values.entrySet()){
            BigDecimal normalizedValue = valueEntry.getValue().divide(normalizator, 20, RoundingMode.HALF_UP);
            normalizedValues.put(valueEntry.getKey(), normalizedValue);
        }
        return normalizedValues;
    }
}
