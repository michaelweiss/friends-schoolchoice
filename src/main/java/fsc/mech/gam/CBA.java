/*
 * 
 * File: 	CBA.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.mech.gam;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fsc.mech.Allocation;
import fsc.mech.Mechanism;
import fsc.model.School;
import fsc.model.Student;
import fsc.model.World;
import fsc.util.RNG;

public abstract class CBA implements Mechanism{

    //Dependency injection for exact rules of the three phases
    protected final Aggregator aggregator;
    protected final Allocator allocator;
    protected final Clusterer clusterer;	

    protected CBA(Aggregator aggregator, Allocator allocator, Clusterer clusterer) {
        this.aggregator = aggregator;
        this.allocator = allocator;
        this.clusterer = clusterer;
    }

    @Override
    public Allocation allocate(World world, RNG rng){
        Set<Cluster> clusters = clusterer.cluster(world.getStudents(), world.getK(), rng);
        Set<RationalCluster> rationalClusters = aggregator.generateSchoolPreferences(clusters);
        return allocator.getAllocation(rationalClusters, rng);
    }


    public static class Cluster extends HashSet<Student>{

        private static final long serialVersionUID = 5024457638734758111L;
        private final World world;

        public Cluster(Collection<Student> collection){
            super(collection);
            if(this.isEmpty()) throw new RuntimeException("must not create empty cluster");
            world = this.iterator().next().getWorld();
        }

        public World getWorld() {
            return this.world;
        }

    }

    public static class RationalCluster extends Cluster{

        private static final long serialVersionUID = 5024457638734758111L;
        private final List<School> ordinalPreferences;

        public RationalCluster(Collection<Student> collection, List<School> ordinalPreferences){
            super(collection);
            this.ordinalPreferences = ordinalPreferences;
        }

        public List<School> getOrdinalPreferences() {
            return Collections.unmodifiableList(ordinalPreferences);
        }
    }


}
