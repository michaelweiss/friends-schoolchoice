/*
 * 
 * File: 	LongestPathST.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.mech.gam;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import fsc.model.Student;
import fsc.util.Pair;
import fsc.util.RNG;

public class LongestPathST implements MinCutClusterer.STFinder {

    @Override
    public Pair<Student> sourceAndSink(Set<Student> students, RNG rng) {
        UndirectedGraph<Student, DefaultEdge> graph = graph(students);
        Set<Student> endNodeCandidates = endNodeCandidate(graph);
        return longestShortestPath(endNodeCandidates, graph);
    }


    private static UndirectedGraph<Student, DefaultEdge> graph(Set<Student> students){
        SimpleGraph<Student, DefaultEdge> friendshipGraph =
                new SimpleGraph<Student, DefaultEdge>
        (DefaultEdge.class);
        for(Student student : students){
            friendshipGraph.addVertex(student);
        }

        for(Student student : students){
            for(Student friend : student.getFriends()){
                // If student and friend are in set of students, add friendship edge
                if(students.contains(friend)){
                    friendshipGraph.addEdge(student, friend);
                }
            }
        }
        return friendshipGraph;
    }

    private static Set<Student> endNodeCandidate(UndirectedGraph<Student, DefaultEdge> graph){
        //		Map<Student, Integer> degrees = new HashMap<>();
        //		for(Student student : graph.vertexSet()){
        //			degrees.put(student, graph.degreeOf(student));
        //		}

        //For the moment, just return all students
        return graph.vertexSet();		
    }


    private static Pair<Student> longestShortestPath(Set<Student> endNodeCandidates, UndirectedGraph<Student, DefaultEdge> graph){
        if(endNodeCandidates.size() < 2) throw new RuntimeException("Not enoght potential s and t");
        if(endNodeCandidates.size() == 2) {
            Iterator<Student> iter = endNodeCandidates.iterator();
            return new Pair<Student>(iter.next(), iter.next());
        }

        Pair<Student> longestPath = null;
        double longestPathLength = Integer.MIN_VALUE;
        List<Student> students = new ArrayList<>(endNodeCandidates);
        for(int i = 0; i < students.size()-1; i++){
            for(int j = i+1; j < students.size(); j++){
                Pair<Student> st = new Pair<Student>(students.get(i), students.get(j));
                double pathLength = shortestPathLength(st, graph);
                if(pathLength > longestPathLength){
                    longestPathLength = pathLength;
                    longestPath = st;
                }
            }
        }

        return longestPath;		
    }

    private static double shortestPathLength(Pair<Student> st, UndirectedGraph<Student, DefaultEdge> graph){
        DijkstraShortestPath<Student, DefaultEdge> dijkstra = new DijkstraShortestPath<Student, DefaultEdge>(graph, st.getFirst(), st.getSecond());
        return dijkstra.getPathLength();
    }

}
