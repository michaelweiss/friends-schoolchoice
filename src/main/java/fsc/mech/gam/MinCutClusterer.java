/*
 * 
 * File: 	LongestPathST.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.mech.gam;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.ConnectivityInspector;
import org.jgrapht.alg.MinSourceSinkCut;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import fsc.model.Student;
import fsc.util.Pair;
import fsc.util.RNG;

public class MinCutClusterer implements Clusterer {


    private final STFinder stFinder;

    //Dependency injection for rules of st-finding
    private MinCutClusterer(STFinder stFinder){
        this.stFinder = stFinder;
    }

    public static MinCutClusterer getRandomSTCutter(){
        return new MinCutClusterer(new RandomST());
    }

    public static MinCutClusterer getLongestPathSTCutter(){
        return new MinCutClusterer(new LongestPathST());
    }

    @Override
    public Set<CBA.Cluster> cluster(Set<Student> students, int k, RNG rng){
        Set<CBA.Cluster> result = new HashSet<CBA.Cluster>();	
        for(Set<Student> connectedStudents : connectedStudents(students)){
            Set<Set<Student>> smallStudentSets = recCutToMinSize(k+1, connectedStudents, rng);
            for(Set<Student> smallStudentSet : smallStudentSets){
                result.add(new CBA.Cluster(smallStudentSet));
            }
        }
        return result;
    }


    public static interface STFinder{
        public Pair<Student> sourceAndSink(Set<Student> students, RNG rng);
    }

    private static DirectedGraph<Student, DefaultEdge> createGraph(Set<Student> students){
        DirectedGraph<Student, DefaultEdge> friendshipGraph =
                new DefaultDirectedGraph<Student, DefaultEdge>
        (DefaultEdge.class);
        for(Student student : students){
            friendshipGraph.addVertex(student);
        }

        for(Student student : students){
            for(Student friend : student.getFriends()){
                // If student and friend are in set of students, add friendship edge
                if(students.contains(friend)){
                    friendshipGraph.addEdge(student, friend);
                }
            }
        }
        return friendshipGraph;
    }

    private static Set<Set<Student>> connectedStudents(Set<Student> students){
        DirectedGraph<Student, DefaultEdge> graph = createGraph(students);
        ConnectivityInspector<Student, DefaultEdge> connectivityInspector = new ConnectivityInspector<>(graph);
        List<Set<Student>> connectedStudentSets = connectivityInspector.connectedSets();
        return new HashSet<Set<Student>>(connectedStudentSets);
    }

    private Set<Set<Student>> recCutToMinSize(int minSize, Set<Student> students, RNG rng){
        Set<Set<Student>> result = new HashSet<Set<Student>>();
        if(students.size() <= minSize){
            //End of recursion condition satisfied
            result.add(students);
            return result;
        }else{
            //Further cut required
            DirectedGraph<Student, DefaultEdge> friendshipGraph = createGraph(students);
            Pair<Student> sourceAndSink = stFinder.sourceAndSink(students, rng);
            MinSourceSinkCut<Student, DefaultEdge> cutter = new MinSourceSinkCut<>(friendshipGraph);
            cutter.computeMinCut(sourceAndSink.getFirst(), sourceAndSink.getSecond());
            Set<Student> sinkCut = cutter.getSinkPartition();
            Set<Student> sourceCut = cutter.getSourcePartition();
            result.addAll(recCutToMinSize(minSize, sinkCut, rng));
            result.addAll(recCutToMinSize(minSize, sourceCut, rng));
            return result;
        }
    }

    public enum STSelector{
        RANDOM, LONGESTPATH
    }


}
