/*
 * 
 * File: 	SplitterMinCutGAM.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package fsc.mech.gam;

public class SplitterMinCutGAM extends CBA {

    private static SplitterMinCutGAM randomSTCutInstance;
    private static SplitterMinCutGAM longestPathCutInstance;
    private final String name;

    private SplitterMinCutGAM(Aggregator aggregator, Allocator allocator,
            Clusterer clusterer, String name) {
        super(aggregator, allocator, clusterer);
        this.name = name;
    }

    public static SplitterMinCutGAM getInstance(MinCutClusterer.STSelector stSelectionType){
        switch(stSelectionType){
        case RANDOM: return getRandomCutInstance();
        case LONGESTPATH: return getLongestPathCutInstance();
        default: throw new RuntimeException("StSplitter not found");
        }
    }

    private static SplitterMinCutGAM getRandomCutInstance(){
        if(randomSTCutInstance == null) {
            randomSTCutInstance = new SplitterMinCutGAM(
                    new ContinuousBordaVoter(), 
                    new SplitterSABMAdapter(), 
                    MinCutClusterer.getRandomSTCutter(),
                    "RandomCutCBA");
        }
        return randomSTCutInstance;
    }

    private static SplitterMinCutGAM getLongestPathCutInstance(){
        if(longestPathCutInstance == null) {
            longestPathCutInstance = new SplitterMinCutGAM(
                    new ContinuousBordaVoter(), 
                    new SplitterSABMAdapter(), 
                    MinCutClusterer.getLongestPathSTCutter(),
                    "MinCutCBA");
        }
        return longestPathCutInstance;
    }

    @Override
    public String getName() {
        return name;
    }




}
