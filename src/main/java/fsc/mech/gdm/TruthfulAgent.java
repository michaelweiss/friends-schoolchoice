/*
 * 
 * File: 	TruthfulAgent.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.mech.gdm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import fsc.mech.Allocation;
import fsc.mech.gdm.GDM.Choice;
import fsc.model.School;
import fsc.model.Student;
import fsc.util.RNG;

public class TruthfulAgent implements GdmAgent{

    private Student student;

    public TruthfulAgent(Student student) {
        this.student = student;
    }

    @Override
    public Student getStudent() {
        return student;
    }

    @Override
    public Choice dictate(List<GdmAgent> unallocatedAgents, Allocation currentAllocation, RNG rng) {
        List<GdmAgent> availableFriends = new ArrayList<GdmAgent>();
        for (GdmAgent agent : unallocatedAgents) {	
            if(student.getFriends().contains(agent.getStudent())){
                availableFriends.add(agent);
            }
        }
        TruthfulAgentChoice bestChoice = null;
        for (School school : currentAllocation.getWorld().getSchools()){
            int remainingCapactiy = currentAllocation.remainingCapacity(school);
            if(remainingCapactiy > 0){
                int friendsToTake;
                if(availableFriends.size() > remainingCapactiy-1){
                    friendsToTake = remainingCapactiy-1; 
                }else{
                    friendsToTake = availableFriends.size();
                }

                int numberOfFriendsAtSchool = 0;
                for(Student allocatedStudent : currentAllocation.getStudents(school)){
                    if(this.student.getFriends().contains(allocatedStudent)) {
                        numberOfFriendsAtSchool++;
                    }
                }
                int friendsAtAllocation = numberOfFriendsAtSchool + friendsToTake;
                BigDecimal potentialValue = this.student.getValue(school, friendsAtAllocation);
                if(bestChoice == null || bestChoice.getValue().compareTo(potentialValue) <= 0){

                    //Create Choice object
                    rng.shuffle(availableFriends);
                    List<GdmAgent> selectedFriendsAgents = availableFriends.subList(0, friendsToTake);
                    bestChoice = new TruthfulAgentChoice(
                            new HashSet<>(selectedFriendsAgents), 
                            school, 
                            friendsAtAllocation,
                            potentialValue);
                }
            }		
        }
        return bestChoice;

    }

    private static class TruthfulAgentChoice extends Choice{

        private final int numberOfFriends;
        private final BigDecimal value;

        public TruthfulAgentChoice(Set<GdmAgent> selectedAgents, School selectedSchool, int numberOfFriends, BigDecimal value) {
            super(selectedAgents, selectedSchool);
            this.numberOfFriends = numberOfFriends;
            this.value = value;
        }

        public int getNumberOfFriends() {
            return numberOfFriends;
        }

        public BigDecimal getValue() {
            return value;
        }

    }


}
