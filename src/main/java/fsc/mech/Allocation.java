/*
 * 
 * File: 	Allocation.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.mech;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fsc.model.School;
import fsc.model.Student;
import fsc.model.World;


public class Allocation{

    private final World world;
    public final Map<School, Set<Student>> allocation;

    public Allocation(World world){
        if(! world.isFinalized() ) {
            throw new RuntimeException("World has to be finalized before starting allocation");
        }
        this.world = world;
        allocation = new HashMap<School, Set<Student>>();
        for(School school: world.getSchools()){
            allocation.put(school, new HashSet<>());
        }
    }

    /**
     * Copy Constructor
     * Shallow-Clones the allocation, (no deep cloning of schools and students).
     */
    public Allocation(Allocation original){
        this(original.getWorld());
        for(Entry<School, Set<Student>> entry : original.allocation.entrySet()){
            for(Student student : entry.getValue()){
                allocate(student, entry.getKey());
            }
        }
    }

    /**
     * Allocates a student to a school
     * @param student
     * @param school
     */
    public void allocate(Student student, School school){
        if(!world.equals(student.getWorld())){
            throw new RuntimeException("Allocation is not for world of this student");
        }
        if(!world.equals(school.getWorld())) {
            throw new RuntimeException("Allocation is not for world of this school");
        }
        if(remainingCapacity(school) < 1) {
            throw new RuntimeException("School does not have enough capacity.");
        }
        allocation.get(school).add(student);
    }

    public int remainingCapacity(School school){
        Set<Student> studentsAtSchool = allocation.get(school);
        if(studentsAtSchool == null ) {
            throw new RuntimeException("School is not known! Possibly not of this world?!");
        }
        return school.getCapacity() - studentsAtSchool.size();
    }

    /**
     * Writes this allocation to file Allocation.bin
     */
    public void print(){
        List<String> fileLines = new ArrayList<String>();
        for(Entry<School, Set<Student>> entry : allocation.entrySet()){
            StringBuilder line = new StringBuilder(entry.getKey().getId());
            line.append(" --> ");
            for(Student student: entry.getValue()){
                line.append(student.getId());
                line.append(", ");
            }
            fileLines.add(line.toString());
        }
        try {
            Files.write(Paths.get("Allocation.bin"), fileLines);
        }catch(Exception e){
            throw new RuntimeException();
        }

    }

    public World getWorld() {
        return world;
    }

    /**
     * Returns the school a given student is allocated to
     * null if the student is not allocated
     * @param student
     * @return
     */
    public School allocatedTo(Student student){
        for(Entry<School, Set<Student>> entry : allocation.entrySet()){
            if(entry.getValue().contains(student)){
                return entry.getKey();
            }
        }
        return null;
    }



    /**
     * Returns an unmodifiable reference to the set of students allocated to this school
     * @param school
     * @return
     */
    public Set<Student> getStudents(School school) {
        Set<Student> students = allocation.get(school);
        if(students == null){
            throw new RuntimeException("School not found. Maybe not same world?");
        }
        return Collections.unmodifiableSet(students);		
    }



}
