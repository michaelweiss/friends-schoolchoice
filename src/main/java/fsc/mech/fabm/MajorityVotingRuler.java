/*
 * 
 * File: 	MajorityVotingRuler.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package fsc.mech.fabm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fsc.mech.Allocation;
import fsc.mech.abm.AllocationRuler;
import fsc.mech.abm.ApplyingAgent;
import fsc.model.School;
import fsc.model.Student;
import fsc.util.RNG;


public class MajorityVotingRuler extends AllocationRuler {

    private Map<Student, GreedyVotingAgent> votingProxies;

    public MajorityVotingRuler(Set<ApplyingAgent> agents) {
        votingProxies = new HashMap<>();
        for(ApplyingAgent applyingAgent : agents){
            //Each applyingAgent consists of exaclty one students
            Student student = applyingAgent.students().iterator().next();
            votingProxies.put(student, new GreedyVotingAgent(student));
        }
    }

    @Override
    public AllocatorResult allocate(Allocation currentAlloc,
            Map<School, Set<ApplyingAgent>> applicants, RNG rng) {
        //Prepare result
        Map<ApplyingAgent, School> newlyAllocated = new HashMap<ApplyingAgent, School>();
        Set<ApplyingAgent> notAllocatedStudents = new HashSet<ApplyingAgent>();

        // Go over all schools with applicants
        for(Map.Entry<School, Set<ApplyingAgent>> entry : applicants.entrySet()){	
            Set<ApplyingAgent> applications = entry.getValue();
            if(applications.size() != 0){	
                if(currentAlloc.remainingCapacity(entry.getKey()) < 1) throw new RuntimeException("School has no capacity but applications. Should not happen in ABM. School:" + entry.getKey().getId());
                School school = entry.getKey();				
                Set<Student> acceptedStudents = currentAlloc.getStudents(school);				
                //Collect Votes
                Map<ApplyingAgent, Integer> votes = new HashMap<ApplyingAgent, Integer>();
                for(ApplyingAgent applicant : applications) votes.put(applicant, 0);

                for(Student acceptedStudent : acceptedStudents){
                    GreedyVotingAgent votingProxy = votingProxies.get(acceptedStudent);
                    for(ApplyingAgent votedAgent : votingProxy.votes(applications)){
                        votes.put(votedAgent, votes.get(votedAgent) + 1);
                    }
                }

                //Apply Majority Voting Rule
                List<ApplyingAgent> randomOrderApplicants = new ArrayList<>();
                randomOrderApplicants.addAll(applications);
                rng.shuffle(randomOrderApplicants);

                int maxVotes = Integer.MIN_VALUE;
                ApplyingAgent currentLeader = null;
                for(ApplyingAgent agent : randomOrderApplicants){
                    if(currentLeader == null || maxVotes < votes.get(agent)){
                        maxVotes = votes.get(agent);
                        currentLeader = agent;
                    }
                }

                // Allocate Winner, Again we simplify by knowing every applyingagent contains exactly one student
                Student winner = currentLeader.students().iterator().next();
                currentAlloc.allocate(winner, school);
                newlyAllocated.put(currentLeader, school);
                for(ApplyingAgent agent : applications){
                    if(!agent.equals(currentLeader)) notAllocatedStudents.add(agent);
                }
            }
        }
        return new AllocatorResult(newlyAllocated, notAllocatedStudents);
    }

}
