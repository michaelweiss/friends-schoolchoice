/*
 * 
 * File: 	FABM.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.mech.fabm;

import java.util.HashSet;
import java.util.Set;

import fsc.mech.Allocation;
import fsc.mech.abm.ABM;
import fsc.mech.abm.AllocationRuler;
import fsc.mech.abm.ApplyingAgent;
import fsc.model.Student;
import fsc.model.World;
import fsc.util.RNG;


public class FABM extends ABM {

    // ============
    // SINGLETON-METHODS
    // ============
    private static FABM INSTANCE;
    private FABM(){}
    public static final FABM getInstance(){
        if(INSTANCE == null) INSTANCE = new FABM();
        return INSTANCE;
    }


    @Override
    protected AllocationRuler createRuler(Set<ApplyingAgent> agents, RNG rng) {
        return new MajorityVotingRuler(agents);
    }

    @Override
    protected Set<ApplyingAgent> createApplyingAgents(World world) {
        Set<ApplyingAgent> agents = new HashSet<ApplyingAgent>();
        for(Student student: world.getStudents()){
            agents.add(new GreedySocialApplyingAgent(student));
        }
        return agents;
    }
    @Override
    protected Allocation treatUnallocatedAgents(
            UnfinishedAllocation unfinishedAllocation, RNG rng) {
        throw new RuntimeException("Not enough capacity for F-ABM. If expected, use a subclass which overrides #treatUnallocatedAgents(...)");		
    }
    @Override
    public String getName() {
        return "FABM";
    }

}
