/*
 * 
 * File: 	ABM.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.mech.abm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import fsc.mech.Allocation;
import fsc.mech.Mechanism;
import fsc.model.School;
import fsc.model.World;
import fsc.util.RNG;


public abstract class ABM implements Mechanism {

    protected abstract AllocationRuler createRuler(Set<ApplyingAgent> agents, RNG rng);
    protected abstract Set<ApplyingAgent> createApplyingAgents(World world);

    @Override
    public Allocation allocate(World world, RNG rng) {
        Allocation emptyAllocation = new Allocation(world);
        Set<ApplyingAgent> agents = createApplyingAgents(world);
        return calculateAllocation(emptyAllocation, agents, rng);
    }

    protected Allocation calculateAllocation(Allocation startAllocation, Set<ApplyingAgent> agents, RNG rng){
        UnfinishedAllocation unfinishedAllocation = calculateUnfinishedAllocation(startAllocation, agents, rng);
        if(unfinishedAllocation.getUnallocatedAgents().isEmpty()){
            return unfinishedAllocation.getAllocation();
        }else{
            return treatUnallocatedAgents(unfinishedAllocation, rng);
        }
    }

    private UnfinishedAllocation calculateUnfinishedAllocation(Allocation startAllocation, Set<ApplyingAgent> agents, RNG rng){
        Allocation allocation = startAllocation;
        Set<ApplyingAgent> yetToAllocateAgents = agents;
        AllocationRuler ruler = createRuler(yetToAllocateAgents, rng);
        Set<ApplyingAgent> rejectedAgents = new HashSet<ApplyingAgent>(); //Set for all agents rejected because of unsatisfying capacity.

        while(!yetToAllocateAgents.isEmpty()){
            // Init new data structure to save appliactions
            Map<School, Set<ApplyingAgent>> applicants = new HashMap<>();
            for(School school : allocation.getWorld().getSchools()) applicants.put(school, new HashSet<>());
            // Collect Applications

            Collection<ApplyingAgent> yetToAllocateCopy = new ArrayList<>(yetToAllocateAgents);
            for(ApplyingAgent agent : yetToAllocateCopy){
                try{
                    School applicationChoice = agent.apply(allocation, rng);
                    applicants.get(applicationChoice).add(agent);
                }catch(NotEnoughCapacityException exception){
                    rejectedAgents.add(agent);
                    yetToAllocateAgents.remove(agent);
                }		
            }
            // Allocate 
            if(!applicants.isEmpty()){
                AllocationRuler.AllocatorResult result = ruler.allocate(allocation, applicants, rng);
                // Treat result
                yetToAllocateAgents.removeAll(result.getNewlyAllocated().keySet());
            }		
        }
        return new UnfinishedAllocation(allocation, rejectedAgents);
    }

    protected abstract Allocation treatUnallocatedAgents(UnfinishedAllocation unfinishedAllocation, RNG rng);

    public static class NotEnoughCapacityException extends Exception{
        private static final long serialVersionUID = 5964002988036607689L;
    }

    public static class UnfinishedAllocation{
        private final Allocation allocation;
        private final Set<ApplyingAgent> unallocatedAgents;

        public UnfinishedAllocation(Allocation allocation,
                Set<ApplyingAgent> unallocatedAgents) {
            this.allocation = allocation;
            this.unallocatedAgents = unallocatedAgents;
        }

        public Allocation getAllocation() {
            return allocation;
        }

        public Set<ApplyingAgent> getUnallocatedAgents() {
            return unallocatedAgents;
        }


    }

}
