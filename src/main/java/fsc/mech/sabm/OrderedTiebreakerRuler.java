/*
 * 
 * File: 	OrderedTiebreakerRuler.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.mech.sabm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fsc.mech.Allocation;
import fsc.mech.abm.AllocationRuler;
import fsc.mech.abm.ApplyingAgent;
import fsc.model.School;
import fsc.model.Student;
import fsc.util.RNG;


public class OrderedTiebreakerRuler extends AllocationRuler{

    // A strict and unmodifyable ordering over the agents used as priority when too many applications for a school, according to Mennle et al (2015)
    private final List<ApplyingAgent> tieBreaker;

    public OrderedTiebreakerRuler(Collection<ApplyingAgent> agents, RNG rng) {
        List<ApplyingAgent> tieBreaker = new ArrayList<ApplyingAgent>(agents);
        rng.shuffle(tieBreaker);
        this.tieBreaker = Collections.unmodifiableList(tieBreaker);
    }

    @Override
    public AllocatorResult allocate(Allocation allocation, Map<School, Set<ApplyingAgent>> applicants, RNG rng) {

        Map<ApplyingAgent, School> newlyAllocated = new HashMap<ApplyingAgent, School>();
        Set<ApplyingAgent> notAllocatedStudents = new HashSet<ApplyingAgent>();

        for(School school : allocation.getWorld().getSchools()){
            List<ApplyingAgent> sortedApplicants = tieBreakingOrdered(applicants.get(school));
            for(ApplyingAgent agent : sortedApplicants){
                if(allocation.remainingCapacity(school) >= agent.agentSize()){
                    // Allocation Possible, allocate
                    for(Student student : agent.students()){
                        allocation.allocate(student, school);				
                    }
                    newlyAllocated.put(agent, school);
                }else{
                    // Allocation not possible, don't allocate
                    notAllocatedStudents.add(agent);
                }
            }		
        }

        return new AllocatorResult(newlyAllocated, notAllocatedStudents);		
    }

    /**
     * Sorts a set of agents by their priority in the tiebreaking-list.
     * @param agents
     * @return
     */
    public List<ApplyingAgent> tieBreakingOrdered(Set<ApplyingAgent> agents){
        //TODO Testing
        // Put applicants in a list
        List<ApplyingAgent> sortedApplicants = new ArrayList<ApplyingAgent>();
        sortedApplicants.addAll(agents);
        // Sort the list
        sortedApplicants.sort(new Comparator<ApplyingAgent>() {

            @Override
            public int compare(ApplyingAgent o1, ApplyingAgent o2) {
                int prioAgent1 = tieBreaker.indexOf(o1);
                int prioAgent2 = tieBreaker.indexOf(o2);
                if(prioAgent1 == -1 || prioAgent2 == -1) throw new RuntimeException("Agent is not in tiebreaking-list. Not of this world?");
                // returns negative if prio of first agent is lower (his later in the tiebreaking list)
                return prioAgent2 - prioAgent1;
            }

        });
        return sortedApplicants;
    }

}
