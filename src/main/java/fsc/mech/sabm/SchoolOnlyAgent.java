/*
 * 
 * File: 	SchoolOnlyAgent.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


package fsc.mech.sabm;

import java.util.List;

import fsc.mech.Allocation;
import fsc.mech.abm.ABM.NotEnoughCapacityException;
import fsc.mech.abm.ApplyingAgent;
import fsc.model.School;
import fsc.util.RNG;

public abstract class SchoolOnlyAgent extends ApplyingAgent {

    // Most-Preferred School First Ordinal Preferences over Schools
    protected abstract List<School> ordinalPreferences();

    @Override
    public School apply(Allocation currentAllocation, RNG rng) throws NotEnoughCapacityException {
        for(School school : ordinalPreferences()){
            if(currentAllocation.remainingCapacity(school) >= agentSize()){
                return school;
            }
        }
        throw new NotEnoughCapacityException();
    }

}
