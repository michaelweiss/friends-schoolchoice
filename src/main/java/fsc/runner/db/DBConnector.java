/*
 * 
 * File: 	DBConnector.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.runner.db;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import fsc.eval.EvaluationReport;
import fsc.model.generation.smallworld.SmallWorldConfig;

public class DBConnector {

    private static String DB_CREDENTIAL_PATH = "DBCredentials";
    private String url;
    private static final boolean isLocal = true;
    private Connection con;
    PreparedStatement smallWorldInsert;
    PreparedStatement evaluationReportInsert;

    public static final int commitBundleSize = 500;

    int queryCount = 0;

    public DBConnector(){
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
            url = readURL();
            con = DriverManager.getConnection(url);
            con.setAutoCommit(false);
            System.out.println("Sucessfully Connected");
            smallWorldInsert = con.prepareStatement("INSERT INTO smallworldsetting VALUES (?,?,?,?,?,?,?,?,?)");
            evaluationReportInsert = con.prepareStatement("INSERT INTO evaluation VALUES (?,?,?,?,?,?,?,?,?,?)");

        } catch (SQLException e) {
            System.err.println("Could not connect to database");
        }

    }


    private static String readURL(){
        try {
            List<String> lines = Files.readAllLines(Paths.get(DB_CREDENTIAL_PATH));
            StringBuilder url = new StringBuilder();
            url.append("jdbc:postgresql://");
            url.append(lines.get(1));
            url.append("?user=");
            url.append(lines.get(3));
            url.append("&password=");
            url.append(lines.get(5));
            return url.toString();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


    }
    private void handleQueryExecution(){
        if(queryCount++ > commitBundleSize){
            queryCount = 0;
            execute();
        }
    }

    public void saveSmallWorld(SmallWorldConfig.FinalSetting setting, String run){
        handleQueryExecution();
        try{
            smallWorldInsert.setInt(1, setting.getId());
            smallWorldInsert.setInt(2, setting.getK());
            smallWorldInsert.setInt(3, setting.getN());
            smallWorldInsert.setInt(4, setting.getM());
            smallWorldInsert.setInt(5, setting.getCapPerSchool());
            smallWorldInsert.setDouble(6, setting.getSchoolValueNoise());
            smallWorldInsert.setDouble(7, setting.getFriendshipImportance().doubleValue());
            smallWorldInsert.setDouble(8, setting.getFriendshipNoise());
            smallWorldInsert.setString(9, run);
            smallWorldInsert.addBatch();
        }catch(SQLException e){

            System.err.println("Could not write smallworlds to database");
        }
    }

    public void saveEvaluation(EvaluationReport report){
        handleQueryExecution();
        try {
            evaluationReportInsert.setInt(1, report.getWorldId());
            evaluationReportInsert.setString(2, report.getMechanism());
            evaluationReportInsert.setDouble(3, report.getNormalizedTotalUtility().doubleValue());
            evaluationReportInsert.setDouble(4, report.getNormalizedMinUtility().doubleValue());
            evaluationReportInsert.setDouble(5, report.getNormalizedMaxUtility().doubleValue());
            evaluationReportInsert.setBigDecimal(6, report.getNashWelfare());
            evaluationReportInsert.setArray(7, con.createArrayOf("int4", report.getSchoolLexicography().getList().toArray()));
            evaluationReportInsert.setArray(8, con.createArrayOf("int4", report.getFriendsLexicography().getList().toArray()));
            evaluationReportInsert.setArray(9, con.createArrayOf("int4", report.getOverallLexicography().getList().toArray()));
            evaluationReportInsert.setInt(10, report.getSetting());

            evaluationReportInsert.addBatch();

        } catch (SQLException e) {

            System.err.println("Could not write smallworlds to database");
        }
    }


    public void execute(){
        try {

            smallWorldInsert.executeBatch();
            evaluationReportInsert.executeBatch();
            con.commit();
        } catch (SQLException e) {
            System.err.println("Could not execute");
        }
    }

    public void close(){
        execute();
        try {
            con.close();
        } catch (SQLException e) {

            System.err.println("Could not close db");
        }
    }

    /**
     * Executes an arbitrary query on the DB
     * Result sets are fetched, i.e. not all lines are immediately called from db, but iteratively
     * See following link for fetching: {@link https://jdbc.postgresql.org/documentation/head/query.html#fetchsize-example}
     * @param query
     * @return
     */
    public void executeQuery(String query, ResultTreatment treatment) {
        Statement st = null;
        ResultSet rs = null;
        try {
            st = con.createStatement();
            //Make sure not all results are retrieved at the same time
            st.setFetchSize(50);
            rs = st.executeQuery(query);
            treatment.treatResult(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if(st != null){
                try {
                    st.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }




    }
}
