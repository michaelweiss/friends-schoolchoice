/*
 * 
 * File: 	DifferentCutsRunner.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.runner;

import java.util.ArrayList;
import java.util.List;

import fsc.eval.EvaluationReport;
import fsc.eval.Evaluator;
import fsc.mech.Allocation;
import fsc.mech.Mechanism;
import fsc.mech.gam.SplitterMinCutGAM;
import fsc.mech.gam.MinCutClusterer.STSelector;
import fsc.model.World;
import fsc.model.WorldFactory;
import fsc.model.generation.smallworld.SmallWorldConfig;
import fsc.model.generation.smallworld.SmallWorldFactory;
import fsc.model.generation.smallworld.SmallWorldConfig.FinalSetting;
import fsc.runner.db.DBConnector;
import fsc.util.JavaUtilRNG;
import fsc.util.RNG;

public class DifferentCutsRunner {

    // Non-Changing Parameters
    private static final int RUNS_PER_SETTING = 10;
    private static final int CAPACITY_PER_SCHOOL = 20;
    private static final int NUMBER_OF_STUDENTS = 300;
    private static final int K = 3;

    private static final String RUNNAME = "DifCut";

    static boolean firstWorld = true;

    public static void main(String args[]){
        SmallWorldConfig config = new SmallWorldConfig(1);

        config.setCapPerSchool(CAPACITY_PER_SCHOOL);
        config.setN(NUMBER_OF_STUDENTS);
        config.setK(K);		

        RNG superRandom = new JavaUtilRNG("SIMULATIONS");

        List<Mechanism> mechanisms = new ArrayList<>();
        mechanisms.add(SplitterMinCutGAM.getInstance(STSelector.LONGESTPATH));
        mechanisms.add(SplitterMinCutGAM.getInstance(STSelector.RANDOM));

        DBConnector db = new DBConnector();

        config.setM( NUMBER_OF_STUDENTS / CAPACITY_PER_SCHOOL);
        config.setFriendshipImportance(0.5);
        config.setSchoolValueNoise(0.5);

        for(int friendshipNoiseStep = 0; friendshipNoiseStep <= 10; friendshipNoiseStep++){
            config.setFriendshipNoise(friendshipNoiseStep * 0.1);

            FinalSetting setting = config.exportSetting();
            //Save world setting in DB
            db.saveSmallWorld(setting, RUNNAME);

            WorldFactory factory = new SmallWorldFactory(setting);	

            for(int run = 0; run < RUNS_PER_SETTING; run++){
                World world = factory.createWorld(superRandom.nextRNG());
                FullJoinRunner.displayProgress(world.getId());
                for(Mechanism mechanism : mechanisms){
                    System.out.println(mechanism.getName());
                    Allocation allocation = mechanism.allocate(world, superRandom.nextRNG());
                    EvaluationReport report = Evaluator.evaluate(allocation, setting.getId(), mechanism.getName());
                    db.saveEvaluation(report);
                }
            }
        }

        db.close();
    }

}
