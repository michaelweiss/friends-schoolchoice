/*
 * 
 * File: 	FullJoinRunner.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.runner;

import java.util.ArrayList;
import java.util.List;

import fsc.eval.EvaluationReport;
import fsc.eval.Evaluator;
import fsc.mech.Allocation;
import fsc.mech.Mechanism;
import fsc.mech.fabm.FABM;
import fsc.mech.gam.SplitterMinCutGAM;
import fsc.mech.gam.MinCutClusterer.STSelector;
import fsc.mech.gdm.TruthfulGDM;
import fsc.mech.sabm.NonSplittingSABM;
import fsc.model.World;
import fsc.model.WorldFactory;
import fsc.model.generation.smallworld.SmallWorldConfig;
import fsc.model.generation.smallworld.SmallWorldConfig.FinalSetting;
import fsc.model.generation.smallworld.SmallWorldFactory;
import fsc.runner.db.DBConnector;
import fsc.util.JavaUtilRNG;
import fsc.util.RNG;

/**
 * Runs the full join of all possible settings
 * @author Michael Weiss
 *
 */
public class FullJoinRunner {

    // Non-Changing Parameters
    private static final int RUNS_PER_SETTING = 20;
    private static final int CAPACITY_PER_SCHOOL = 20;
    private static final int NUMBER_OF_STUDENTS = 300;
    private static final int K = 3;

    private static final String RUNNAME = "Full";

    static boolean firstWorld = true;

    public static void main(String args[]){
        SmallWorldConfig config = new SmallWorldConfig(30000);
        World.setStartId(300000);

        config.setCapPerSchool(CAPACITY_PER_SCHOOL);
        config.setN(NUMBER_OF_STUDENTS);
        config.setK(K);		

        RNG superRandom = new JavaUtilRNG("FULLJOINSIMULATIONS");

        List<Mechanism> mechanisms = new ArrayList<>();
        mechanisms.add(FABM.getInstance());
        mechanisms.add(NonSplittingSABM.getInstance());
        mechanisms.add(SplitterMinCutGAM.getInstance(STSelector.RANDOM));
        mechanisms.add(TruthfulGDM.getInstance());


        DBConnector db = new DBConnector();


        for(int m = NUMBER_OF_STUDENTS / CAPACITY_PER_SCHOOL; m < NUMBER_OF_STUDENTS / CAPACITY_PER_SCHOOL * 1.5; m+=2){
            config.setM(m);

            for(int friendshipImportanceStep = 0; friendshipImportanceStep <= 4; friendshipImportanceStep++){
                config.setFriendshipImportance(friendshipImportanceStep * 0.25);

                for(int friendshipNoiseStep = 0; friendshipNoiseStep <= 4; friendshipNoiseStep++){
                    config.setFriendshipNoise(friendshipNoiseStep * 0.25);

                    for(int schoolNoiseStep = 0; schoolNoiseStep <= 4; schoolNoiseStep++){
                        config.setSchoolValueNoise(schoolNoiseStep * 0.25);
                        FinalSetting setting = config.exportSetting();
                        //Save world setting in DB
                        db.saveSmallWorld(setting, RUNNAME);

                        WorldFactory factory = new SmallWorldFactory(setting);	

                        for(int run = 0; run < RUNS_PER_SETTING; run++){
                            World world = factory.createWorld(superRandom.nextRNG());
                            displayProgress(world.getId());
                            for(Mechanism mechanism : mechanisms){
                                Allocation allocation = mechanism.allocate(world, superRandom.nextRNG());
                                EvaluationReport report = Evaluator.evaluate(allocation, setting.getId(), mechanism.getName());
                                db.saveEvaluation(report);
                            }

                        }
                    }
                }
            }			
        }

        db.close();
    }


    public static void displayProgress(int worldNumber){
        //		if(firstWorld){
        //			System.out.print("Progres: Currently doing world 00000000");
        //			firstWorld = false;
        //		}else{
        //			System.out.print("\b\b\b\b\b\b\b");
        //			String newNumber = "00000000".substring(0,7 - worldNumber/10 + 1).concat(String.valueOf(worldNumber));
        //			System.out.print(newNumber);
        //		}

        if(worldNumber % (DBConnector.commitBundleSize / RUNS_PER_SETTING) == 0) System.out.println("World " + worldNumber );
    }

}
