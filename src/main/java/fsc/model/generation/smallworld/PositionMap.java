/*
 * 
 * File: 	PositionMap.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.model.generation.smallworld;

import java.io.Serializable;

import fsc.util.RNG;


public class PositionMap {

    //Hide Constructor, as only static methods
    private PositionMap(){};

    public final static double GRIDXMAX = 1;
    public final static double GRIDXMIN = 0;
    public final static double GRIDYMAX = 1;
    public final static double GRIDYMIN = 0;
    public final static double MAXDISTANCE 
    = getDistance(new Position(GRIDXMAX, GRIDYMAX), new Position(GRIDXMIN, GRIDYMIN));

    public interface OnMap{
        Position getPosition();
    }

    /**
     * Straight-line distance between the two positions
     * @param position1
     * @param position2
     * @return
     */
    public static double getDistance(Position position1, Position position2){
        return Math.sqrt(Math.pow(position1.getX()-position2.getX(), 2) + Math.pow(position1.getY()-position2.getY(), 2));
    }

    public static boolean isOnMap(Position position){
        if(position.getX() > GRIDXMAX || position.getX() < GRIDXMIN || position.getY() > GRIDYMAX || position.getY() < GRIDYMIN){
            return false;
        }
        return true;
    }

    public static class Position implements Serializable{

        private final double x;
        private final double y;

        public Position(RNG random) {
            this.x = random.nextUniformDouble(GRIDXMIN, GRIDXMAX);
            this.y = random.nextUniformDouble(GRIDYMIN, GRIDYMAX);
        }

        public Position(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public double getX() {
            return x;
        }
        public double getY() {
            return y;
        }


    }

    public static double getNormalizedDistance(Position position1,
            Position position2) {
        double distance = getDistance(position1, position2);
        return distance / MAXDISTANCE;
    }
}
