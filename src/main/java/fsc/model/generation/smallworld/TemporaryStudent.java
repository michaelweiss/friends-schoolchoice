/*
 * 
 * File: 	TemporaryStudent.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.model.generation.smallworld;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import fsc.model.School;
import fsc.model.Student;
import fsc.model.World;
import fsc.util.RNG;
import fsc.util.ValuePair;

public class TemporaryStudent extends Student{

    private static final long serialVersionUID = 1205112749647825934L;
    private List<Student> potentialFriends = new ArrayList<Student>();



    public TemporaryStudent(World world, Clique clique, Map<Integer, BigDecimal> friendshipValues, double schoolValueNoise, BigDecimal friendshipImportance, RNG rng) {
        super(world,  friendshipValues,
                generateUSchools(clique, schoolValueNoise, friendshipImportance, rng));
    }

    private static Map<School, BigDecimal> generateUSchools(Clique clique, Double schoolValueNoise, BigDecimal friendshipImportance, RNG rng){
        Map<School, BigDecimal> schoolValues = new HashMap<School, BigDecimal>();
        for(Entry<School, BigDecimal> baseValue : clique.generateUSchools().entrySet()){
            BigDecimal schoolValueImportance = BigDecimal.ONE.subtract(friendshipImportance);
            BigDecimal randomInfluence = rng.uniformRandomInfluence(schoolValueNoise);
            BigDecimal actualValue = baseValue.getValue().multiply(randomInfluence).multiply(schoolValueImportance);
            schoolValues.put(baseValue.getKey(), actualValue);
        }
        Map<School, BigDecimal> strictSchoolValues = makeStrictRoundedUtilities(schoolValues, rng);
        return strictSchoolValues;
    }


    @Override
    public void addFriend(Student potentialFriend){
        if(this.equals(potentialFriend)) throw new RuntimeException("Student was forced to be his own imagniary friend and refused to continue in simulation :) ");
        if(isFinalizedStudent()) throw new RuntimeException("Tried to add friend to finalized student");
        if(!world.equals(potentialFriend.getWorld())) throw new RuntimeException("Friends are not part of the same world");
        //Only add to potential students and decide about actual friendship once student is finalized
        potentialFriends.add(potentialFriend);
    }

    @Override
    public void finalizeStudent(RNG rng){
        //Randomly decide which friends to take
        rng.shuffle(potentialFriends);
        Iterator<Student> potentialFriendsIter = potentialFriends.iterator();
        for(int i = 0; i < world.getK() && potentialFriendsIter.hasNext(); i++){
            super.addFriend(potentialFriendsIter.next());
        }
        super.finalizeStudent(rng);
    }


    public static Map<Integer, BigDecimal> generateFriendshipValues(int k, BigDecimal friendshipImportance){
        Map<Integer, BigDecimal> friendshipValues = new HashMap<Integer, BigDecimal>();
        for(int i = 0; i <= k; i++){
            double baseValue = Math.log(i+1)/Math.log(k+1);
            BigDecimal value = friendshipImportance.multiply(new BigDecimal(baseValue));
            friendshipValues.put(i, value);
        }
        return friendshipValues;
    }

    /**
     * Rounds all the utilities and induces strict preferences.
     * @param preferences
     * @param rng
     * @return
     */
    public static Map<School, BigDecimal> makeStrictRoundedUtilities(Map<School, BigDecimal> preferences, RNG rng){
        Map<School, BigDecimal> roundedValues = new HashMap<School, BigDecimal>();
        // Round the original values
        BigDecimal small_epsilon = BigDecimal.valueOf(1, 10).divide(
                new BigDecimal(preferences.size()+1),
                15, RoundingMode.HALF_UP);
        for(Entry<School, BigDecimal> entry : preferences.entrySet()){
            BigDecimal rounded = entry.getValue().setScale(10, BigDecimal.ROUND_HALF_UP);
            roundedValues.put(entry.getKey(), rounded);
        }
        // Create strict preferences
        List<School> ordinalWithIndifferency = ValuePair.ordinal(preferences);
        List<School> sameValueSchools = new ArrayList<School>();
        BigDecimal currentValue = BigDecimal.valueOf(Long.MAX_VALUE);
        Map<School, BigDecimal> result = new HashMap<School, BigDecimal>();
        for(School school : ordinalWithIndifferency){
            if(roundedValues.get(school).compareTo(currentValue) < 0){
                //New value, store all with previously same value
                rng.shuffle(sameValueSchools);
                for(int i = 0; i < sameValueSchools.size(); i++){
                    BigDecimal factor = small_epsilon.multiply(new BigDecimal(i));
                    BigDecimal value = roundedValues.get(school).add(factor);
                    result.put(sameValueSchools.get(i), value);
                }
                sameValueSchools = new ArrayList<School>();	
                sameValueSchools.add(school);
                currentValue = roundedValues.get(school);
            }else{
                // Same value as before
                sameValueSchools.add(school);
            }
        }
        // Store School Values of last iteration
        rng.shuffle(sameValueSchools);
        for(int i = 0; i < sameValueSchools.size(); i++){
            BigDecimal factor = small_epsilon.multiply(new BigDecimal(i));
            BigDecimal value = roundedValues.get(sameValueSchools.get(i)).add(factor);
            result.put(sameValueSchools.get(i), value);
        }

        return result;		
    }
}


