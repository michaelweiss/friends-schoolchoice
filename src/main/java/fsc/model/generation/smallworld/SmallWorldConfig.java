/*
 * 
 * File: 	SmallWorldConfig.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.model.generation.smallworld;

import java.math.BigDecimal;


public class SmallWorldConfig {


    private int id_count;

    private int n = Integer.MIN_VALUE;
    private int k = Integer.MIN_VALUE;
    private int m = Integer.MIN_VALUE;
    private int capPerSchool = Integer.MIN_VALUE;
    private double schoolValueNoise = Double.NEGATIVE_INFINITY;
    private BigDecimal friendshipImportance = BigDecimal.valueOf(-1);
    private double friendshipNoise = Double.NEGATIVE_INFINITY;


    public SmallWorldConfig(int startId){
        id_count = startId;
    }

    private int createID(){
        return id_count++;
    }

    public int getN() {
        return n;
    }
    public void setN(int n) {		
        this.n = n;
    }
    public int getK() {
        return k;
    }
    public void setK(int k) {
        this.k = k;
    }
    public int getM() {
        return m;
    }
    public void setM(int m) {
        this.m = m;
    }
    public int getCapPerSchool() {
        return capPerSchool;
    }
    public void setCapPerSchool(int capPerSchool) {
        this.capPerSchool = capPerSchool;
    }
    public double getSchoolValueNoise() {
        return schoolValueNoise;
    }
    public void setSchoolValueNoise(double schoolValueNoise) {
        this.schoolValueNoise = schoolValueNoise;
    }
    public BigDecimal getFriendshipImportance() {
        return friendshipImportance;
    }
    public void setFriendshipImportance(double d) {
        this.friendshipImportance = BigDecimal.valueOf(d);
    }
    public double getFriendshipNoise() {
        return friendshipNoise;
    }
    public void setFriendshipNoise(double friendshipNoise) {
        this.friendshipNoise = friendshipNoise;
    }

    public FinalSetting exportSetting(){
        return new FinalSetting(this);
    }

    public class FinalSetting{

        private final int n;
        private final int k;
        private final int m;
        private final int capPerSchool;
        private final double schoolValueNoise;
        private final BigDecimal friendshipImportance;
        private final double friendshipNoise;
        private final int id;

        private FinalSetting(SmallWorldConfig config){
            this.id = SmallWorldConfig.this.createID();
            this.n = config.getN();
            this.k = config.getK();
            this.m = config.getM();
            this.capPerSchool = config.getCapPerSchool();
            this.schoolValueNoise = config.getSchoolValueNoise();
            this.friendshipImportance = config.getFriendshipImportance();
            this.friendshipNoise = config.getFriendshipNoise();
            checkValidity();
        }

        private void checkValidity(){
            if(n < 0){
                throw new RuntimeException("Misconfig");
            }
            if(k < 0){
                throw new RuntimeException("Misconfig");
            }
            if(m < 0){
                throw new RuntimeException("Misconfig");
            }
            if(capPerSchool < 0){
                throw new RuntimeException("Misconfig");
            }
            if(schoolValueNoise < 0){
                throw new RuntimeException("Misconfig");
            }
            if(friendshipImportance.compareTo(BigDecimal.ZERO) < 0){
                throw new RuntimeException("Misconfig");
            }
            if(friendshipNoise < 0){
                throw new RuntimeException("Misconfig");
            }
        }


        public int getId() {
            return id;
        }

        public int getN() {
            return n;
        }

        public int getK() {
            return k;
        }

        public int getM() {
            return m;
        }

        public int getCapPerSchool() {
            return capPerSchool;
        }

        public double getSchoolValueNoise() {
            return schoolValueNoise;
        }

        public BigDecimal getFriendshipImportance() {
            return friendshipImportance;
        }

        public double getFriendshipNoise() {
            return friendshipNoise;
        }


    }




}
