/*
 * 
 * File: 	SmallWorldFactory.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.model.generation.smallworld;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import fsc.model.World;
import fsc.util.RNG;
import fsc.model.WorldFactory;
import fsc.model.generation.smallworld.PositionMap.Position;
import fsc.model.generation.smallworld.SmallWorldConfig.FinalSetting;

public class SmallWorldFactory implements WorldFactory{



    private final FinalSetting config;

    public SmallWorldFactory(SmallWorldConfig.FinalSetting config) {
        this.config = config;
        if(config.getN() % config.getK() != 0) throw new RuntimeException("Only generating full-clique instances");
    }

    @Override
    public World createWorld(RNG rng) {
        World world = new World(config.getK());
        Set<LocationSchool> schools = new HashSet<LocationSchool>();
        for(int i = 0; i < config.getM(); i++){
            LocationSchool newSchool = new LocationSchool(config.getCapPerSchool(), world, new Position(rng));
            world.addSchool(newSchool);
            schools.add(newSchool);
        }
        world.finalizeSchools();

        LinkedList<Clique> cliques = new LinkedList<>();
        Map<Integer, BigDecimal> friendsValues = TemporaryStudent.generateFriendshipValues(config.getK(), config.getFriendshipImportance());
        for(int i = 0; i< config.getN(); i++){
            // If required, create new clique
            if(i % config.getK() == 0) cliques.addFirst(new Clique(config.getK(), schools, new Position(rng), config.getFriendshipNoise()));
            // Create new student, add it to world and clique
            TemporaryStudent student = new TemporaryStudent(world, cliques.getFirst(), friendsValues, config.getSchoolValueNoise(), config.getFriendshipImportance(), rng.nextRNG());
            world.addStudent(student);
            cliques.getFirst().add(student);
        }
        world.finalizeStudentList();

        for(Clique clique : cliques){
            //Determine Friendships
            clique.assignFriendsToAllStudents(world.getStudents(), rng.nextRNG());
        }
        world.finalizeAllStudents(rng);

        return world;
    }

}
