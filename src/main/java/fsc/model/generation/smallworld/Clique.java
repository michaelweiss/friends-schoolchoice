/*
 * 
 * File: 	Clique.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.model.generation.smallworld;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fsc.model.School;
import fsc.model.Student;
import fsc.model.generation.smallworld.PositionMap.OnMap;
import fsc.model.generation.smallworld.PositionMap.Position;
import fsc.util.RNG;


public class Clique extends HashSet<Student> implements OnMap{

    private static final long serialVersionUID = -7822714583556621177L;
    private final int k;
    private boolean assignendFriends;
    private final Set<LocationSchool> schools;
    private final double friendshipNoise;
    private static final double schoolValueBase = Math.E;

    private final Position position;

    public Clique(int k, Set<LocationSchool> schools, Position position, double friendshipNoise){
        this.schools = schools;
        this.k = k;
        this.assignendFriends = false;
        this.position = position;
        this.friendshipNoise = friendshipNoise;
    }

    @Override
    public Position getPosition(){
        return this.position;
    }

    /**
     * Generates a set of schoolvalues, without random influences. Should be taken by student and be randomized
     * @param clique
     * @return
     */
    public Map<School, BigDecimal> generateUSchools(){
        Map<School, BigDecimal> baseValues = new HashMap<School, BigDecimal>();
        for(LocationSchool school : this.getSchools()){
            double normalizedDistance = PositionMap.getNormalizedDistance(this.getPosition(), school.getPosition());
            double value = Math.pow(schoolValueBase, -1 * normalizedDistance);
            baseValues.put(school, new BigDecimal(value));
        }
        return baseValues;
    }

    @Override
    public boolean add(Student student){
        if(this.size() >= k) throw new RuntimeException("Clique has at least max size, can't add student");
        if(this.assignendFriends) throw new RuntimeException("Already assigned friends, can't add more students to clique");
        return super.add(student);
    }

    public void assignFriendsToAllStudents(Set<Student> allstudents, RNG rng){
        List<Student> ordererdStudents = new ArrayList<>(this);
        rng.shuffle(ordererdStudents);
        for(int firstStudent = 0; firstStudent < ordererdStudents.size()-1; firstStudent++){
            for(int secondStudent = firstStudent+1; secondStudent< ordererdStudents.size(); secondStudent++){
                double draw = rng.nextDouble();
                if(draw <= friendshipNoise){
                    // Assign a friendship between these two students
                    ordererdStudents.get(firstStudent).addFriend(ordererdStudents.get(secondStudent));
                    ordererdStudents.get(secondStudent).addFriend(ordererdStudents.get(firstStudent));
                }else{
                    // Assign a frienship between firststudent and an outside student
                    List<Student> allStudentsCopy = new ArrayList<>(allstudents);
                    allStudentsCopy.removeAll(this);
                    Student newFriend = allStudentsCopy.get(rng.nextInt(allStudentsCopy.size()));
                    ordererdStudents.get(firstStudent).addFriend(newFriend);
                    newFriend.addFriend(ordererdStudents.get(firstStudent));
                }
            }
        }	
        assignendFriends = true;
    }

    public Set<LocationSchool> getSchools() {
        return schools;
    }
}
