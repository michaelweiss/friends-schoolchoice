/*
 * 
 * File: 	World.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.model;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import fsc.util.RNG;


public class World implements Serializable{

    private static final long serialVersionUID = -2291002103756329463L;
    private static int ID_COUNT=0;
    private final int k;

    private final Set<Student> students;
    private boolean studentListFinalized;
    private final Set<School> schools;
    private boolean schoolsFinalized;
    private final int id;

    public static void setStartId(int startId){
        ID_COUNT = startId;
    }

    public World(int k) {
        super();
        this.k = k;
        this.students = new HashSet<>();
        this.schools = new HashSet<>();
        this.studentListFinalized = false;
        this.schoolsFinalized = false;
        this.id = getNewId();
    }

    private static int getNewId(){
        return ID_COUNT++;
    }

    public int getK() {
        return k;
    }

    public boolean isStudentListFinalized() {
        return studentListFinalized;
    }

    /**
     * Finalizes the Student List, such that no further students can be added.
     */
    public void finalizeStudentList() {
        this.studentListFinalized = true;
    }

    /**
     * Finalizes all students, such that no further friendships can be added.
     * @param rng
     */
    public void finalizeAllStudents(RNG rng){
        if (!isStudentListFinalized()){
            throw new RuntimeException("Students not yet finalized in this world");
        }
        for(Student student : students){
            student.finalizeStudent(rng);
        }
    }

    public boolean isSchoolsFinalized() {
        return schoolsFinalized;
    }

    public void finalizeSchools() {
        this.schoolsFinalized = true;
    }

    public boolean isFinalized(){
        return studentListFinalized && schoolsFinalized;
    }

    public void addSchool(School school){
        if(isSchoolsFinalized()) {
            throw new RuntimeException("Cannot add School. Schools in this World are finalized");
        }
        schools.add(school);
    }

    public Set<Student> getStudents() {
        if (!isStudentListFinalized()){
            throw new RuntimeException("Students not yet finalized in this world");
        }
        return Collections.unmodifiableSet(students);
    }

    public void addStudent(Student student){
        if(!isSchoolsFinalized()){
            throw new RuntimeException("Cannot add Student. Schools in this World are not yet finalized");
        }
        if (isStudentListFinalized()) {
            throw new RuntimeException("Cannot add Student. Students in this World are finalized");
        }
        students.add(student);
    }

    public Set<School> getSchools() {
        if (!isSchoolsFinalized()){
            throw new RuntimeException("Schools not yet finalized in this world");
        }
        return Collections.unmodifiableSet(schools);
    }

    public int getId() {
        return id;
    }


}
