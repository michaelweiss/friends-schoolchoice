/*
 * 
 * File: 	Student.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import fsc.mech.Allocation;
import fsc.util.RNG;
import fsc.util.ValuePair;

public class Student implements Serializable {

    private static final long serialVersionUID = 7526882635788109204L;

    private static int ID_COUNT = 0;

    protected final int id;
    protected final World world;
    protected final Set<Student> friends;
    //unmodifiable
    protected final Map<Integer, BigDecimal> uFriends;
    //unmodifiable
    protected final Map<School, BigDecimal> uSchools;

    // Used for normalization
    private final BigDecimal maxValue;
    private final BigDecimal minValue;
    private final BigDecimal maxMinValueDifference;

    private BigDecimal sumDistinctValues;

    private boolean isFinalizedStudent;

    public Student(World world, Map<Integer, BigDecimal> uFriends, Map<School, BigDecimal> uSchools){
        this.id = getNewId();
        this.world = world;
        this.friends = new HashSet<>();
        this.uFriends = Collections.unmodifiableMap(uFriends);
        this.uSchools = Collections.unmodifiableMap(uSchools);	

        // Calculate normalization params
        List<School> schoolsOrdinal = ValuePair.ordinal(uSchools);
        BigDecimal maxSchoolValue = uSchools.get(schoolsOrdinal.get(0));
        BigDecimal minSchoolValue = uSchools.get(schoolsOrdinal.get(schoolsOrdinal.size()-1));
        BigDecimal maxFriendsValue = uFriends.get(world.getK());
        BigDecimal minFriendsValue = uFriends.get(0);
        sumDistinctValues = null;

        this.maxValue = maxFriendsValue.add(maxSchoolValue);
        this.minValue = minFriendsValue.add(minSchoolValue);	
        this.maxMinValueDifference = maxValue.subtract(minValue);
    }

    private BigDecimal calculateSumDistinctValues(){
        BigDecimal result = BigDecimal.ZERO;
        for(School school : uSchools.keySet()){
            for(int i = 0; i <= world.getK(); i++){
                int friendsNumber;
                if(i > friends.size()){
                    //Student does not have this many friends, enter the amount for the hightest possible number again 
                    //(as n times the amount of allocations with this possible number of friends at the same school)
                    friendsNumber = friends.size();
                }else{
                    friendsNumber = i;
                }
                result = result.add(getValue(school, friendsNumber));
            }
        }
        return result;
    }

    private static int getNewId(){
        return ID_COUNT++;
    }

    public boolean isFinalizedStudent() {
        return isFinalizedStudent;
    }

    /**
     * Finalize the student
     * No more friends can be added to this student after calling the {@link #finalizeStudent()} method.
     */
    public void finalizeStudent(RNG rng) {
        this.isFinalizedStudent = true;
    }

    /**
     * Adds a new friendship to this student
     * @param friend
     */
    public void addFriend(Student friend){
        if(this.equals(friend)){
            throw new RuntimeException("Student was forced to be his own imagniary friend and refused to continue in simulation :) ");
        }
        if(world.getK() == friends.size()){
            throw new RuntimeException("Tried to add more than k friends");
        }
        if(isFinalizedStudent()){
            throw new RuntimeException("Tried to add friend to finalized student");
        }
        if(!world.equals(friend.getWorld())){
            throw new RuntimeException("Friends are not part of the same world");
        }
        this.friends.add(friend);
    }

    public int getId() {
        return id;
    }

    public World getWorld() {
        return world;
    }

    public Set<Student> getFriends() {
        return Collections.unmodifiableSet(friends);
    }

    public BigDecimal getUtility(int numberOfFriends) {
        return uFriends.get(numberOfFriends);
    }

    public BigDecimal getUtility(School school) {
        return uSchools.get(school);
    }


    /**
     * Returns the values for all schools 
     * @return
     */
    public Map<School, BigDecimal> getuSchools() {
        return Collections.unmodifiableMap(uSchools);
    }

    /**
     * The value the student percieves of a given allocation
     * null if student is not allocated
     * @param a
     * @return
     */
    public BigDecimal getValue(Allocation allocation){
        School school = allocation.allocatedTo(this);
        if(school == null) return null;
        Set<Student> costudents = allocation.getStudents(school);
        return getValue(school, costudents);
    }

    /**
     * The normalized value the student percieves of a given allocation
     * null if student is not allocated
     * Normalization: Value is 1 for most valued allocation, 0 for least valued allocation
     * @param a
     * @return
     */
    public BigDecimal getNormalizedValue(Allocation allocation){
        if(sumDistinctValues == null) sumDistinctValues = calculateSumDistinctValues();
        BigDecimal value = getValue(allocation);
        //		if (value == null) return value;
        //		if (maxMinValueDifference.compareTo(BigDecimal.ZERO) == 0) {
        //			System.out.println("Warning: Min Value equals Max Value");
        //			return BigDecimal.ONE; 
        //		}
        //		return value.subtract(minValue).divide(maxMinValueDifference, 13, BigDecimal.ROUND_HALF_UP);
        return value.divide(sumDistinctValues,  13, BigDecimal.ROUND_HALF_UP);
    }

    /**
     * The value the student would have if allocated to a specific school, given his currently allocated friends
     * 
     * @param allocation
     * @param school
     * @return
     */
    public BigDecimal getPotentialValue(Allocation allocation, School school){
        if(allocation.remainingCapacity(school) < 1) return null;
        Set<Student> costudents = allocation.getStudents(school);
        return getValue(school, costudents);
    }

    /**
     * 
     * @param school
     * @param numberOfFriends: Must not be bigger than the actual number of friends this student has.
     * @return
     */
    public BigDecimal getValue(School school, int numberOfFriends){
        if(numberOfFriends > friends.size()) {
            throw new RuntimeException("Not enough friends for request");
        }
        BigDecimal utility = getUtility(numberOfFriends).add(getUtility(school));
        return utility;
    }

    private BigDecimal getValue(School school, Set<Student> costudents){
        int numberOfFriends = getNumberOfFriends(costudents);
        return getValue(school, numberOfFriends);
    }

    /**
     * Returns the number of friends of this student in set potentialFriends
     * @param potentialFriends
     * @return
     */
    public int getNumberOfFriends(Set<Student> potentialFriends){
        int numberOfFriends = 0;
        for(Student friend : friends){
            if(potentialFriends.contains(friend)){
                numberOfFriends++;
            }
        }
        return numberOfFriends;
    }

    /** 
     * Returns the allocation rank for this student and a given allocation, as used in the overall lexicographic 
     * @param allocation
     * @return
     */
    public int allocationRank(Allocation allocation){
        BigDecimal allocValue = getValue(allocation);
        int rank = 0;
        List<School> orderedSchools = ValuePair.ordinal(uSchools);
        int maxFriends = world.getK() > friends.size() ? friends.size() : world.getK();
        for(School school : orderedSchools){
            boolean allSmaller = true;
            for(int friends = maxFriends; friends >= 0; friends-- ){
                if(getValue(school, friends).compareTo(allocValue) > 0){
                    rank++;
                    allSmaller = false;
                }
            }
            if(allSmaller) {
                return rank;
            }
        }
        return rank;
    }




}
