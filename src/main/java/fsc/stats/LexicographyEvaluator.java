/*
 * 
 * File: 	LexicographyEvaluator.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.stats;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import fsc.eval.Lexicography;
import fsc.runner.db.ResultTreatment;

public class LexicographyEvaluator {


    /**
     * 
     * @param resultSet
     * @param lexicoColumnName
     * @param paramName
     * @param mechnanisms = as used for column names of lexicography
     * @return
     * @throws SQLException
     */
    public static List<String> evaluation(ResultSet resultSet, String paramName, List<String> mechanisms) throws SQLException{
        List<String> result = new ArrayList<String>();
        //Create Header line
        StringBuilder header = new StringBuilder(paramName);
        for(String mechanim : mechanisms){
            header.append(',').append(mechanim);
        }
        result.add(header.toString());

        // ============
        // Calculate Average Ranks
        // ============

        // Ranks per World
        Set<Map<String, Integer>> ranksPerWorldAndMech = new HashSet<Map<String,Integer>>();
        Double currentParam = null;
        while(resultSet.next()){
            //Check if new Param
            Double rsParam = resultSet.getDouble(paramName);
            if(currentParam == null) {
                currentParam = rsParam;
            }else if(currentParam.compareTo(rsParam) != 0){
                result.add(getFileLine(currentParam, ranksPerWorldAndMech, mechanisms));
                ranksPerWorldAndMech = new HashSet<Map<String,Integer>>();
                currentParam = rsParam;
            }
            Map<String, Lexicography> lexicos = new HashMap<>();
            for(String mechanism : mechanisms){
                Lexicography lexi = new Lexicography(resultSet.getArray(mechanism));
                lexicos.put(mechanism, lexi);
            }
            Map<String, Integer> ranks = calculateRanks(lexicos);
            //			System.out.println(resultSet.getInt("worldId") + "  " + ranks.toString());
            ranksPerWorldAndMech.add(ranks);
        }
        resultSet.close();
        //Treat last param setting
        result.add(getFileLine(currentParam, ranksPerWorldAndMech, mechanisms));
        return result;
    }

    private static Map<String, Integer> calculateRanks(Map<String, Lexicography> lexicos) {
        //Slow impl, as #mech low no problem
        List<Lexicography> ordered = new ArrayList<>(lexicos.values());
        Collections.sort(ordered); //Sort worst --> best lexico
        Collections.reverse(ordered); // Sort best --> worst lexico

        //Determine Ranks, thanks to Lexicography.equals and .hashCode, first equiv lexi is taken
        Map<String, Integer> ranks = new HashMap<String, Integer>();
        for(Entry<String, Lexicography> entry : lexicos.entrySet()){
            ranks.put(entry.getKey(), ordered.indexOf(entry.getValue()));
        }

        return ranks;
    }

    /**
     * Calculates AVG Ranks and returns file line
     * @param param
     * @param ranksPerWorldAndMech
     * @param mechOrder
     * @return
     */
    private static String getFileLine(
            Double param, Set<Map<String, Integer>> ranksPerWorldAndMech, List<String> mechOrder) {
        //Calc AVG
        Map<String, Double> averages = new HashMap<String, Double>();
        for(String mech : mechOrder){
            int rankSum = 0;
            for(Map<String, Integer> ranks : ranksPerWorldAndMech){
                rankSum += ranks.get(mech);
            }
            int numberOfWorlds = ranksPerWorldAndMech.size();
            double avg = rankSum / (double) numberOfWorlds;
            averages.put(mech, avg);
        }

        //Create line for File
        StringBuilder line = new StringBuilder(String.valueOf(param));
        for(String mech : mechOrder){
            line.append(',');
            line.append(String.valueOf(averages.get(mech)));
        }
        return line.toString();
    }

    protected static class LexicoResultTreatment implements ResultTreatment{

        private final List<String> mech;

        public LexicoResultTreatment(String[] mechanisms) {
            mech = Arrays.asList(mechanisms);
        }

        @Override
        public void treatResult(ResultSet rs) {
            try {
                List<String> report = evaluation(rs, "friendshipImportance", mech);		
                for(int i = 0; i < report.size(); i++){
                    System.out.println(report.get(i));
                }
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }			
        }




    }
}
