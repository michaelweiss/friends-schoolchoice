/*
 * 
 * File: 	SchoolLexicoEval.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.stats.runner;

import fsc.runner.db.DBConnector;
import fsc.stats.LexicographyEvaluator;

public class SchoolLexicoEval extends LexicographyEvaluator {

    /**
     * Evaluates SchoolLexicoEval according to specified query and writes outcome to sys.out
     * @param args
     */
    public static void main(String[] args) {
        String query = "select  worldid, friendshipImportance, "
                + "max(case when mechanism = 'regularSABM' then lexischool end) sabm, "
                + "max(case when mechanism = 'FABM' then lexischool end) fabm, "
                + "max(case when mechanism = 'RandomCutCBA' then lexischool end) cut, "
                + "max(case when mechanism = 'TruthGDM' then lexischool end) gdm "
                + " from evaluation, smallworldsetting"
                + " where id = settingid and run=\'FIXED\'  and schoolnoise = 0.5 and friendshipNoise = 0.25 and m=15 "
                + " group by worldid, friendshipImportance"
                + " order by friendshipimportance asc";

        String[] mechanisms = {"sabm", "fabm", "cut", "gdm"};
        LexicoResultTreatment treatment = new LexicoResultTreatment(mechanisms);
        (new DBConnector()).executeQuery(query, treatment);
    }

}
