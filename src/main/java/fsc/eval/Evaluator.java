/*
 * 
 * File: 	Evaluator.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.eval;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;

import fsc.mech.Allocation;
import fsc.model.School;
import fsc.model.Student;
import fsc.model.World;
import fsc.util.ValuePair;


public final class Evaluator {

    // Prevent unrequired instantiation
    private Evaluator(){};

    public static EvaluationReport evaluate(Allocation allocation, int settingid, String mechanism){
        return new EvaluationReport(allocation, settingid, mechanism);
    }


    /**
     * Evaluated social welfare with respect to total percieved utility without normalization
     * @param allocation
     */
    public static BigDecimal evaluateUnnormalizedWelfare(Allocation allocation){

        World world = allocation.getWorld();

        BigDecimal totalUtility = BigDecimal.ZERO;
        for(Student student : world.getStudents()){
            totalUtility = totalUtility.add(student.getValue(allocation));
        }

        return totalUtility;
    }


    /**
     * Evaluated social welfare with respect to total percieved utility
     * @param allocation
     */
    public static BigDecimal evaluateNormalizedWelfare(Allocation allocation){

        World world = allocation.getWorld();

        BigDecimal totalUtility = BigDecimal.ZERO;
        for(Student student : world.getStudents()){
            totalUtility = totalUtility.add(student.getNormalizedValue(allocation));
        }

        return totalUtility;
    }

    /**
     * Evaluates friendship lexicography
     * @param allocation
     * @return
     */
    public static Lexicography evaluateFriendships(Allocation allocation){

        World world = allocation.getWorld();

        Lexicography friendsLexicography = new Lexicography(world.getK() + 1);
        for(Student student: world.getStudents()){
            School school = allocation.allocatedTo(student);
            Set<Student> atSameSchool = allocation.getStudents(school);
            int numberOfFriends = student.getNumberOfFriends(atSameSchool);
            int lexicographicPosition = world.getK()-numberOfFriends;
            friendsLexicography.increaseByOne(lexicographicPosition);
        }

        return friendsLexicography;
    }

    /**
     * Evaluates school lexicography
     * @param allocation
     * @return
     */
    public static Lexicography evaluateSchool(Allocation allocation){
        World world = allocation.getWorld();

        Lexicography schoolLexicography = new Lexicography(world.getSchools().size());
        for(Student student : world.getStudents()){
            School school = allocation.allocatedTo(student);
            List<School> ordinalPreferences = ValuePair.ordinal(student.getuSchools());
            int rank = ordinalPreferences.indexOf(school);
            schoolLexicography.increaseByOne(rank);
        }

        return schoolLexicography;
    }

    /**
     * Evaluates Overall Lexicography
     * @param allocation
     * @return
     */
    public static Lexicography evaluateOverall(Allocation allocation){
        World world = allocation.getWorld();
        Lexicography lexicography = new Lexicography((world.getK()+1)*world.getSchools().size());
        for(Student student : world.getStudents()){
            int rank = student.allocationRank(allocation);
            lexicography.increaseByOne(rank);
        }
        return lexicography;
    }
}
