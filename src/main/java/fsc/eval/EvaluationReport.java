/*
 * 
 * File: 	EvaluationReport.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.eval;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fsc.mech.Allocation;
import fsc.model.Student;

public class EvaluationReport {

    private Lexicography friendsLexicography;
    private Lexicography schoolLexicography;
    private Lexicography overallLexicography;
    private List<BigDecimal> utilities;

    private BigDecimal minUtility = BigDecimal.TEN;
    private BigDecimal maxUtility = BigDecimal.ZERO;
    private BigDecimal nashWelfare = BigDecimal.ONE;
    private BigDecimal totalUtility = BigDecimal.ZERO;

    private BigDecimal welfareLowerLimit = BigDecimal.valueOf(1, 900);

    private final int worldId;
    private final int setting;


    private final String mechanism;

    public EvaluationReport(Allocation allocation, Integer setting, String mechanism) {
        this.friendsLexicography = Evaluator.evaluateFriendships(allocation);
        this.schoolLexicography = Evaluator.evaluateSchool(allocation);
        this.overallLexicography = Evaluator.evaluateOverall(allocation);
        this.worldId = allocation.getWorld().getId();
        this.mechanism = mechanism;
        this.setting = setting;
        evaluateUtilities(allocation);
    }

    public Lexicography getOverallLexicography() {
        return overallLexicography;
    }

    public void setOverallLexicography(Lexicography overallLexicography) {
        this.overallLexicography = overallLexicography;
    }

    public Lexicography getFriendsLexicography() {
        return friendsLexicography;
    }

    public Lexicography getSchoolLexicography() {
        return schoolLexicography;
    }

    public void setFriendsLexicography(Lexicography friendsLexicography) {
        this.friendsLexicography = friendsLexicography;
    }

    public void setSchoolLexicography(Lexicography schoolLexicography) {
        this.schoolLexicography = schoolLexicography;
    }


    public List<BigDecimal> getNormalizedUtilities() {
        return utilities;
    }

    public BigDecimal getNormalizedMinUtility() {
        return minUtility;
    }

    public BigDecimal getNormalizedMaxUtility() {
        return maxUtility;
    }

    public BigDecimal getNashWelfare() {
        return nashWelfare;
    }

    public BigDecimal getNormalizedTotalUtility() {
        return totalUtility;
    }

    /**
     * Evluates Minimum Utility, Maximum Utility, Nash Utility and Total Utility and stores all if it in this Evaluation Report. 
     * @param allocation
     */
    public void evaluateUtilities(Allocation allocation) {
        List<Student> students = new ArrayList<>(allocation.getWorld().getStudents());
        Collections.sort(students, (Student student1, Student student2) -> student1.getId()-student2.getId());
        minUtility = BigDecimal.TEN;
        maxUtility = BigDecimal.ZERO;
        nashWelfare = BigDecimal.ONE;
        totalUtility = BigDecimal.ZERO;
        utilities = new ArrayList<>();

        for (Student student : students){
            BigDecimal value = student.getNormalizedValue(allocation);
            value = value.round(MathContext.DECIMAL32);
            totalUtility = totalUtility.add(value);
            //			nashWelfare = nashWelfare.multiply(value.add(BigDecimal.ONE));
            nashWelfare = nashWelfare.multiply(value);
            //			System.out.println(nashWelfare.toEngineeringString());
            if(value.compareTo(maxUtility) > 0){
                maxUtility = value;
            }
            if(value.compareTo(minUtility) < 0){
                minUtility = value;
            }
            utilities.add(value);
        }
        if(nashWelfare.compareTo(welfareLowerLimit) < 0){
            nashWelfare = BigDecimal.ZERO;
        }
    }

    public int getWorldId() {
        return worldId;
    }

    public String getMechanism() {
        return mechanism;
    }	

    public int getSetting() {
        return setting;
    }



}
