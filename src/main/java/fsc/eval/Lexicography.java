/*
 * 
 * File: 	Lexicography.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.eval;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class Lexicography implements Comparable<Lexicography>{

    //Most prefered first lexicography
    List<Integer> lexicographic;

    /**
     * size many, possible outcomes. Initially, all values are 0
     * @param size
     */
    public Lexicography(Integer size) {
        this.lexicographic = new ArrayList<Integer>(size);
        for(int i = 0; i< size; i++){
            lexicographic.add(0);
        }
    }

    public Lexicography(Array dbArray){
        try {
            Integer[] values = (Integer[])dbArray.getArray();
            this.lexicographic = Arrays.asList(values);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public List<Integer> getList(){
        return Collections.unmodifiableList(lexicographic);
    }

    /**
     * Set the value for the p-th most desired outcome, with p starting at 0
     * Example: If 4 students get k-1 student allocated, the call would be setEntry(1, 4)
     * @return 
     */
    public void setEntry(int p, int value){
        lexicographic.set(p, value);
    }

    public int getEntry(int p){
        return lexicographic.get(p);
    }

    public void increaseByOne(int p){
        lexicographic.set(p, lexicographic.get(p)+1);
    }

    public int getSize(){
        return lexicographic.size();
    }


    @Override
    public String toString(){
        return lexicographic.toString();
    }

    @Override
    public int compareTo(Lexicography otherLexicography) {
        if(this.getSize() != otherLexicography.getSize()) throw new RuntimeException("Unequal Size");

        for(int p = getSize()-1; p >= 0; p--){
            if(otherLexicography.getEntry(p) < getEntry(p)){
                // Other Lexicography is preferable
                return -1;
            }else if(otherLexicography.getEntry(p) > getEntry(p)){
                // This Lexicography is preferable
                return 1;
            }
        }
        return 0;
    }


    @Override
    public boolean equals(Object o){
        if(o instanceof Lexicography){
            Lexicography other = (Lexicography) o;
            if(other.getSize() != this.getSize()) return false;
            return (other.compareTo(this) == 0);
        }
        return false;
    }

    @Override
    public int hashCode(){
        int result = 1;
        for(Integer i : lexicographic){
            result =  32 * result + i;
        }
        return result;
    }


}
