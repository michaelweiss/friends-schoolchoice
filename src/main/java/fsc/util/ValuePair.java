/*
 * 
 * File: 	ValuePair.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ValuePair<T> {

    private final BigDecimal value;
    private final T key;


    public ValuePair( T key, BigDecimal value) {
        this.value = value;
        this.key = key;
    }


    /**
     * Sorts a list of valuepairs in decreasing order
     * @param listToSort
     * @return
     */
    public static <Q> List<ValuePair<Q>> sortDecreasing(List<ValuePair<Q>> listToSort){
        //TODO Test Correct Functionning
        List<ValuePair<Q>> result = new ArrayList<ValuePair<Q>>(listToSort);
        Collections.sort(result, new Comparator<ValuePair<Q>>() {
            @Override
            public int compare(ValuePair<Q> pairOne, ValuePair<Q> pairTwo) {
                return pairOne.value.compareTo(pairTwo.getValue());
            }
        });
        Collections.reverse(result);
        return result;
    }

    /**
     * Returns list the keys of a Map, sorted by their BigDecimal-Value in decreasing order
     * @param input
     * @return
     */
    public static <Q> List<Q> ordinal(Map<Q, BigDecimal> input){
        List<ValuePair<Q>> valuePairs = new ArrayList<>();
        for(Entry<Q, BigDecimal> entry : input.entrySet()){
            valuePairs.add(new ValuePair<Q>(entry.getKey(), entry.getValue()));
        }
        //Sort List
        valuePairs = sortDecreasing(valuePairs);
        List<Q> result = new ArrayList<>();
        for(ValuePair<Q> pair : valuePairs){
            result.add(pair.getKey());
        }
        return result;		
    }

    public BigDecimal getValue() {
        return value;
    }

    public T getKey() {
        return key;
    }





}
