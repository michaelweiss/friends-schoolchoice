/*
 * 
 * File: 	JavaUtilRNG.java
 * Author:	Michael Weiss (weiss.michael@gmx.ch)
 * Date:	19.11.2015
 *
 * Copyright (c) 2015, Michael Weiss and NICTA
 * All rights reserved.
 *
 * Developed by: Michael Weiss
 *               weiss.michael@gmx.ch
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of NICTA nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY NICTA ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NICTA BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package fsc.util;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class JavaUtilRNG extends RNG {

    private static final long serialVersionUID = 1593307899607626758L;
    private final Random childrenGenerator;
    private final Random rng;


    public JavaUtilRNG(String startSeed){
        this(startSeed.hashCode());
    }

    protected JavaUtilRNG(long seed){
        super(seed);
        this.rng = new Random(seed);
        this.childrenGenerator = new Random(rng.nextLong());
    }

    /* (non-Javadoc)
     * @see fsc.util.RNG#uniformRandomInfluence(double)
     */	
    @Override
    public BigDecimal uniformRandomInfluence(double noise){
        return new BigDecimal(nextUniformDouble(1-noise/2, 1+noise/2));
    }

    /* (non-Javadoc)
     * @see fsc.util.RNG#nextUniformDouble(double, double)
     */
    @Override
    public double nextUniformDouble(double min, double max){
        return rng.nextDouble()*(max-min)+min;
    }

    @Override
    public RNG nextRNG() {
        return new JavaUtilRNG(childrenGenerator.nextLong());
    }

    @Override
    public void shuffle(List<?> list) {
        Collections.shuffle(list, rng);

    }

    @Override
    public int nextInt(int maxExclusive) {
        return rng.nextInt(maxExclusive);
    }

    @Override
    public double nextDouble() {
        return rng.nextDouble();
    }

}
